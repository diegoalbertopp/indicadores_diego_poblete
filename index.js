const fs = require('fs');
const fetch = require('node-fetch');
const readline = require('readline');
const actualizarFuncion = require('./librerias/descarga.js');
const consultarPromedio = require('./librerias/lecturaescritura.js');
//const minimoHistorico = require('./librerias/lecturaescritura.js');

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

menu();

function menu() {
    console.log('Menu');
    console.log('1. Actualizar indicadores');
    console.log('2. Promediar');
    console.log('3. Mostrar valor más actual');
    console.log('4. Mostrar mínimo histórico');
    console.log('5. Mostrar máximo histórico');
    console.log('6. Salir\n');
    lector.question('Escriba su respuesta: ', opcion => {
        switch (opcion) {
            case '1':
                console.log('\nOpción 1\n');
                actualizarFuncion();
                break;
            case '2':
                console.log('\nOpción 2\n');
                console.log('1. Dolar');
                console.log('2. Euro');
                console.log('3. Tasa de desempleo');
                consultarPromedio();
                break;
            case '3':
                console.log('\nOpción 3\n');
                menu();
                break;
            case '4':
                console.log('\nOpción 4\n');
                //minimoHistorico();
                break;
            case '5':
                console.log('\nOpción 5\n');
                menu();
                break;
            case '6':
                console.log('\nAdios\n');
                lector.close();
                break;
            default:
                console.log('\nOpción ingresada no es válida, intente nuevamnete\n');
                menu();
        }
    });

}