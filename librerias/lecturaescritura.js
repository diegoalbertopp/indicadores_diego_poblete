const fs = require('fs');

function consultarPromedio() {
    var archivos = [];
    fs.readdirSync('./datos/').forEach(file => {
        archivos.push(file);
    });

    let promesa = new Promise((resolve) => {
        var sumaUS = 0;
        var sumaEUR = 0;
        var sumaTD = 0;
        var promedioUS = 0;
        var promedioEUR = 0;
        var promedioTD = 0;

        for (var i = 0; i < archivos.length; i++) {
            if (i < archivos.length - 1) {
                fs.readFile('./datos/' + archivos[i], 'utf-8', (err, data) => {
                    sumaUS = sumaUS + JSON.parse(data).dolar.valor;
                    sumaEUR = sumaEUR + JSON.parse(data).euro.valor;
                    sumaTD = sumaTD + JSON.parse(data).tasa_desempleo.valor;
                });
            } else {
                fs.readFile('./datos/' + archivos[i], 'utf-8', (err, data) => {
                    sumaUS = sumaUS + JSON.parse(data).dolar.valor;
                    sumaEUR = sumaEUR + JSON.parse(data).euro.valor;
                    sumaTD = sumaTD + JSON.parse(data).tasa_desempleo.valor;
                    promedioUS = sumaUS / i;
                    promedioEUR = sumaEUR / i;
                    promedioTD = sumaTD / i;
                    resolve(console.log("\nPromedio Dolar: " + promedioUS + "\nPromedio Euro: " + promedioEUR + "\nPromedio Tasa de desempleo: " + promedioTD));
                });
            }
        }
    })
};

// function minimoHistorico() {
//     var archivos = [];
//     fs.readdirSync('./datos/').forEach(file => {
//         archivos.push(file);
//     });

//     let promesa = new Promise((resolve) => {
//         var valorUS = 0;
//         var valorEUR = 0;
//         var valorTD = 0;

//         for (var i = 0; i < archivos.length; i++) {
//             fs.readFile('./datos/' + archivos[i], 'utf-8', (err, data) => {
//                 valorUS = JSON.parse(data).dolar.valor;
//                 valorEUR = JSON.parse(data).euro.valor;
//                 valorTD = JSON.parse(data).tasa_desempleo.valor;
//                 if (i == archivos.length - 1) {
//                     resolve(console.log("\nMínimo Histórico\nDolar: " + valorUS + "\Euro: " + valorEUR + "\Tasa de desempleo: " + valorTD));
//                 }
//             });
//         }
//     })
// };

module.exports = consultarPromedio;
// module.exports = minimoHistorico;