const fetch = require('node-fetch');
const fs = require('fs');
const timestamp = require('time-stamp');
var lecturaFuncion = require('../librerias/lecturaescritura.js');

function actualizarFuncion() {
    const status = response => {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response);
        }

        return Promise.reject(new Error(response.statusText));
    };
    const obtenerJson = response => {
        return response.json();
    }

    const filtrarDatos = json => {
        return new Promise((resolve, reject) => {
            var fecha = timestamp('DD-MM-YY');
            var hora = timestamp('HH:mm:ss');
            var info = '{"fecha":' + JSON.stringify(fecha) + ',';
            info += '"hora":' + JSON.stringify(hora) + ',';
            for (var elem in json) {

                if (elem === 'dolar' || elem === 'euro') {
                    info += JSON.stringify(elem) + ':' + JSON.stringify(json[elem]) + ',';
                }
                if (elem === 'tasa_desempleo') {
                    info += JSON.stringify(elem) + ':' + JSON.stringify(json[elem]);
                }
            }
            info += '}';
            console.log(json);
            resolve(JSON.parse(info));
        });
    }

    fetch('https://mindicador.cl/api')
        .then(status)
        .then(obtenerJson)
        .then(filtrarDatos)
        .then((info) => {
            fs.writeFile('./datos/' + timestamp('DDMMYYYY-HHmmss') + '.ind', JSON.stringify(info), (err) => { console.error(err) });
        })
        .catch(console.error);

}


module.exports = actualizarFuncion;